package numbertowords

import (
	"strconv"
	"strings"
)

// Convert converts given number to text bath
// default lang = "en"
func Convert(number float64, lang string, baht ...bool) (string, error) {
	switch strings.ToUpper(lang) {
	case th:
		lang = th
	case en:
		lang = en
	default:
		lang = en
	}

	flagTextBaht := false
	if v := len(baht); v > 0 {
		flagTextBaht = baht[0]
	}

	return convert(number, lang, flagTextBaht)
}

func convert(number float64, lang string, baht bool) (string, error) {
	switch lang {
	case th:
		return fullWordsTH(number)
	default:
		switch baht {
		case true:
			return fullWordsBahtEN(number)
		default:
			return fullWordsEN(number)
		}
	}
}

func getInteger(number float64) string {
	return strings.Split(strconv.FormatFloat(number, 'f', 2, 64), ".")[0]
}

func getDecimal(number float64) string {
	return strings.Split(strconv.FormatFloat(number, 'f', 2, 64), ".")[1]
}
