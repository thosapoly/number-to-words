package numbertowords

import (
	"strconv"
	"testing"
)

func TestBahtIntegerEN(t *testing.T) {
	testCases := []struct {
		input  string
		output string
	}{
		{"0", "zero"},
		{"5", "five"},
		{"11", "eleven"},
		{"15", "fifteen"},
		{"88", "eighty eight"},
		{"44", "fourty four"},
		{"111", "onehundred eleven"},
		{"555", "fivehundred fifty five"},
		{"515", "fivehundred fifteen"},
		{"4414", "fourthousand fourhundred fourteen"},
		{"15512", "fifteenthousand fivehundred twelve"},
		{"52152", "fifty twothousand onehundred fifty two"},
		{"15155", "fifteenthousand onehundred fifty five"},
		{"141512", "onehundred fourty onethousand fivehundred twelve"},
		{"1111111", "onemillion onehundred eleventhousand onehundred eleven"},
		{"22222222", "twenty twomillion twohundred twenty twothousand twohundred twenty two"},
		{"88888888", "eighty eightmillion eighthundred eighty eightthousand eighthundred eighty eight"},
	}

	for _, tc := range testCases {
		t.Run(tc.input, func(t *testing.T) {
			got, err := integerEN(tc.input, en)
			if err != nil {
				t.Errorf("Error occurred while converting integer to words: %v", err)
			}

			if got != tc.output {
				t.Errorf("got %v, expect %v", got, tc.output)
			}
		})
	}
}

func TestBathDecimalEN(t *testing.T) {
	testCases := []struct {
		input  string
		output string
	}{
		{"00", "Only"},
		{"01", "and One Stang"},
		{"10", "and Ten Stang"},
		{"15", "and Fifteen Stang"},
		{"20", "and Twenty Stang"},
		{"25", "and Twenty Five Stang"},
	}

	for _, tc := range testCases {
		t.Run(tc.input, func(t *testing.T) {
			got, err := decimalBahtEN(tc.input, en)
			if err != nil {
				t.Errorf("Error occurred while converting decimal to words: %v", err)
			}

			if got != tc.output {
				t.Errorf("got %v, expect %v", got, tc.output)
			}
		})
	}
}

func TestBahtConvertEN(t *testing.T) {
	testCases := []struct {
		input  float64
		output string
	}{
		{0.00, "Zero Baht Only"},
		{0.25, "Twenty Five Stang"},
		{9.99, "Nine Baht and Ninety Nine Stang"},
		{10.00, "Ten Baht Only"},
		{22.22, "Twenty Two Baht and Twenty Two Stang"},
		{88.88, "Eighty Eight Baht and Eighty Eight Stang"},
		{111.11, "One Hundred Eleven Baht and Eleven Stang"},
		{9850.15, "Nine Thousand Eight Hundred Fifty Baht and Fifteen Stang"},
		{1243.71, "One Thousand Two Hundred Fourty Three Baht and Seventy One Stang"},
		{11100.56, "Eleven Thousand One Hundred Baht and Fifty Six Stang"},
		{51100.56, "Fifty One Thousand One Hundred Baht and Fifty Six Stang"},
		{511003.56, "Five Hundred Eleven Thousand Three Baht and Fifty Six Stang"},
	}

	for _, tc := range testCases {
		t.Run(strconv.FormatFloat(tc.input, 'f', 2, 64), func(t *testing.T) {
			got, err := Convert(tc.input, en, true)
			if err != nil {
				t.Errorf("Error occurred while converting: %v", err)
			}

			if got != tc.output {
				t.Errorf("got %v, expect %v", got, tc.output)
			}
		})
	}
}

func BahtBenchmarkConvertEN(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Convert(111.11, en, true)
	}
}
