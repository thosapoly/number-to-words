package numbertowords

import (
	"fmt"
	"math"
	"strconv"
	"strings"
)

func fullWordsBahtEN(number float64) (string, error) {
	var (
		integer = getInteger(number)
		decimal = getDecimal(number)
	)

	integerWord, err := integerBahtEN(integer, en)
	if err != nil {
		return "", err
	}

	decimalWord, err := decimalBahtEN(decimal, en)
	if err != nil {
		return "", err
	}

	fw := fmt.Sprintf("%s Baht %s", integerWord, decimalWord)
	fw = strings.Replace(fw, "Zero Baht and ", "", -1)

	return fw, nil
}

func integerBahtEN(str, lang string) (string, error) {
	integer, err := strconv.ParseFloat(str, 64)
	if err != nil {
		return "", err
	}

	if isSmallNumber(int(integer)) {
		return strings.Title(getSmallNumber(int(integer), lang)), nil
	}

	integerLength := len(str)

	wordList := make([]string, 0)

	for idx, c := range str {
		if integer == 0.00 {
			break
		}

		var (
			word string
			num  = string(c)
		)

		if isSmallNumber(int(integer)) {
			word = getSmallNumber(int(integer), lang)
			integer = 0.00
		} else {
			digitKey := integerLength - idx - 1

			switch digitKey {
			case tenThousandDigitKey, hundredThousandDigitKey, tenMillionDigitKey:
				continue
			case thousandDigitKey:
				if integerLength >= 6 {
					num = string(str[idx-2]) + string(str[idx-1]) + string(c)
				} else if integerLength >= 5 {
					num = string(str[idx-1]) + string(c)
				}
			case millionDigitKey:
				if integerLength >= 9 {
					num = string(str[idx-2]) + string(str[idx-1]) + string(c)
				} else if integerLength >= 8 {
					num = string(str[idx-1]) + string(c)
				}
			}

			numKey, err := strconv.ParseFloat(num, 64)
			if err != nil {
				panic(err)
			}

			if numKey == 0.00 {
				continue
			}

			integer -= numKey * math.Pow(10, float64(digitKey))

			word = getBahtWord(int(numKey), digitKey, lang)
		}

		wordList = append(wordList, strings.Title(word))
	}

	return strings.Replace(strings.Join(wordList, " "), "  ", " ", -1), nil
}

func decimalBahtEN(str, lang string) (string, error) {
	if str == "00" {
		return "Only", nil
	}

	wordList := make([]string, 0)
	wordList = append(wordList, "and")

	integerWord, err := integerEN(str, lang)
	if err != nil {
		return "", err
	}
	wordList = append(wordList, strings.Title(integerWord))
	wordList = append(wordList, "Stang")

	return strings.Join(wordList, " "), nil
}
